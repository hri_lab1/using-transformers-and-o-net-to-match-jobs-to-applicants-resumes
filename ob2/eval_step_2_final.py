from cv_analyzer import *


filename = "eval_data.json"
data_file = open(filename, encoding='utf-8')
data = json.loads(data_file.read())
data_file.close()

for i, cv in enumerate(data):
    posts = cv.get("job_posts")
    posts_predicted_categories = []
    for post in posts:
        text1 = post.get("job_description")
        text2 = post.get("job_requirements")
        if text1 is not None:
            text = text1 + " - "
        else:
            text = ""
        if text2 is not None:
            text += text2
        post["predicted_categories"] = [x[4] for x in cv_get_results(text)]
        print(post["predicted_categories"])
        posts_predicted_categories.append(post["predicted_categories"][0])
    cv["posts'_predicted_categories"] = posts_predicted_categories

    resume = cv["resume"]
    jobs = cv["post_categories"]
    jobs.extend(posts_predicted_categories)
    result = cv_get_results(resume, jobs=jobs)
    cv["predicted_post_categories"] = [x[4] for x in result]
    cv["annotation1"] = ""
    cv["annotation2"] = ""
    print(i, result)

with open('eval_data.json', "w", encoding="utf-8") as text_file:
    print(json.dumps(data), file=text_file)
