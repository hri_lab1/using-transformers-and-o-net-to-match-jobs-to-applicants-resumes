import json

import torch
from sentence_transformers import SentenceTransformer
from sentence_transformers.util import cos_sim

settings = {"model": "all-mpnet-base-v2", "skills": "skill_embeddings.pt", "jobs": "titles_embeddings.pt",
            "tasks": "task_embeddings.pt", "tools": "tools_embeddings.pt"}

# settings = {"model": "all-MiniLM-L6-v2", "skills": "skill_embeddings-2.pt", "jobs": "titles_embeddings-2.pt",
# "tasks": "task_embeddings-2.pt", "tools": "tools_embeddings-2.pt"}

model = SentenceTransformer(settings["model"], device='cuda' if torch.cuda.is_available() else "cpu")


def get_st_job_titles(jobs):
    if len(jobs) == 0:
        return []

    jobs_titles = torch.load(settings["jobs"])

    titles = jobs_titles["titles"]
    codes = jobs_titles["codes"]
    emb_sentences = jobs_titles["emb_titles"]
    embeddings = model.encode(jobs)

    scores = cos_sim(embeddings, emb_sentences)
    maximus = torch.max(scores, 1)
    indices = [int(x) for x in maximus.indices]
    obt_titles = [titles[x] for x in indices]
    obt_codes = [codes[x] for x in indices]
    codes = [[codes[x], titles[x]] for x in indices]
    results = {}
    for i in range(len(jobs)):
        results[jobs[i]] = [obt_titles[i], obt_codes[i]]

    results["codes"] = codes
    print(obt_titles)
    return obt_titles


filename = "eval_data.json"
data_file = open(filename, encoding='utf-8')
data = json.loads(data_file.read())
data_file.close()

new_data = []

for i, cv in enumerate(data):
    pr = get_st_job_titles(cv["post_categories"])
    new_cv = {}
    new_cv["resume"] = cv["resume"]
    new_cv["resume_category"] = cv["resume_category"]
    new_cv["job_posts"] = cv["job_posts"]
    new_cv["post_categories"] = cv["post_categories"][:10]
    new_cv["predicted_post_categories"] = cv["posts'_predicted_categories"]
    new_cv["baseline_1"] = cv["baseline_1"]
    new_cv["baseline1_annotation1"] = ""
    new_cv["baseline1_annotation2"] = ""
    new_cv["baseline_2"] = cv["baseline_2"]
    new_cv["baseline2_annotation1"] = ""
    new_cv["baseline2_annotation2"] = ""
    new_cv["index_of_associated_post"] = [pr.index(x) % 10 for x in cv["predicted_post_categories"]]
    new_cv["predicted_categories_for_curriculum-post_association"] = cv["predicted_post_categories"]
    new_cv["annotation1"] = ""
    new_cv["annotation2"] = ""
    print(i, cv["predicted_post_categories"])
    new_data.append(new_cv)

with open('eval_data2.json', "w", encoding="utf-8") as text_file:
    print(json.dumps(new_data), file=text_file)
