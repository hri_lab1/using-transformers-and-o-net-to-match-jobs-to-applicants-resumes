import json
# from math import ceil
from math import dist

from sentence_transformers import SentenceTransformer
from sentence_transformers.util import cos_sim
import torch
from blob_parser import Parser

p = Parser()
threshold = 0.65

settings = {"model": "all-mpnet-base-v2", "skills": "skill_embeddings.pt", "jobs": "titles_embeddings.pt",
            "tasks": "task_embeddings.pt", "tools": "tools_embeddings.pt"}
# settings = {"model": "all-MiniLM-L6-v2", "skills": "skill_embeddings-2.pt", "jobs": "titles_embeddings-2.pt",
# "tasks": "task_embeddings-2.pt", "tools": "tools_embeddings-2.pt"}


model = SentenceTransformer(settings["model"], device='cuda' if torch.cuda.is_available() else "cpu")


def baseline1():
    filename = "test_data.json"
    data_file = open(filename, encoding='utf-8')
    data = json.loads(data_file.read())
    data_file.close()
    for i, res in enumerate(data):
        print(i)
        resume_text = res.get("resume")
        jobs = res.get("post_categories")
        orig_embedding = model.encode(jobs)

        resume = p.get_data(resume_text)
        resume_embedding = model.encode(resume["nouns"] + resume["nouns phrases"] + resume["sentences"])
        st_scores = cos_sim(orig_embedding, resume_embedding)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]
        result = []
        for k, name in enumerate(jobs):
            result.append([name, m[k]])
        scores = sorted(result, key=lambda x: x[1], reverse=True)

        print(scores[:5])
        res["baseline_1"] = [x[0] for x in scores[:5]]

    with open('baseline1.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(data), file=text_file)


def vector_calc(v, v2, embedding, embedding2):
    st_scores = []
    for emb in v:
        st_score = cos_sim(emb, embedding)
        maximus = torch.max(st_score, 1)
        m = [1 if float(x) > threshold else 0 for x in maximus.values]
        st_scores.append(m)

    for emb in v2:
        st_score = cos_sim(emb, embedding2)
        maximus = torch.max(st_score, 1)
        m = [1 if float(x) > threshold else 0 for x in maximus.values]
        st_scores.append(m)

    return st_scores


def calc_distance(resume_st_score, post_st_score):
    distance = 0
    for i, v in enumerate(resume_st_score):
        d = dist(v, post_st_score[i])
        distance += d
    return distance


def baseline2():
    filename = "baseline1.json"
    data_file = open(filename, encoding='utf-8')
    data = json.loads(data_file.read())
    data_file.close()

    tech_skill_embeddings = torch.load('baseline2/all_skills_embeddings.pt')
    tools_embeddings = torch.load('baseline2/all_tools_embeddings.pt')
    tasks_embeddings = torch.load('baseline2/all_tasks_embeddings.pt')

    skills_names_embeddings = torch.load('baseline2/skill_names_embeddings.pt')
    skills_names2_embeddings = torch.load('baseline2/skill_names2_embeddings.pt')

    knowledge_names_embeddings = torch.load('baseline2/knowledge_names_embeddings.pt')
    knowledge_names2_embeddings = torch.load('baseline2/knowledge_names2_embeddings.pt')

    abilities_names_embeddings = torch.load('baseline2/abilities_names_embeddings.pt')
    abilities_names2_embeddings = torch.load('baseline2/abilities_names2_embeddings.pt')

    work_activities_names_embeddings = torch.load('baseline2/work_activities_names_embeddings.pt')
    work_activities_names2_embeddings = torch.load('baseline2/work_activities_names2_embeddings.pt')

    v = [tech_skill_embeddings, tools_embeddings,  skills_names_embeddings, knowledge_names_embeddings,
         abilities_names_embeddings,  work_activities_names_embeddings]
    v2 = [tasks_embeddings, skills_names2_embeddings, knowledge_names2_embeddings, abilities_names2_embeddings,
          work_activities_names2_embeddings]

    for i, res in enumerate(data):
        print(i)
        post_categories = res.get("post_categories")
        resume_text = res.get("resume")
        resume = p.get_data(resume_text)

        resume_embedding = model.encode(resume["nouns"] + resume["nouns phrases"])
        resume_embedding2 = model.encode(resume["sentences"])

        resume_st_score = vector_calc(v, v2, resume_embedding, resume_embedding2)

        distances = []
        job_posts = res.get("job_posts")

        for j, job_post in enumerate(job_posts):
            text1 = job_post.get("job_description")
            text2 = job_post.get("job_requirements")
            if text1 is not None:
                text = text1 + " - "
            else:
                text = ""
            if text2 is not None:
                text += text2
            post = p.get_data(text)
            post_embedding = model.encode(post["nouns"] + post["nouns phrases"])
            post_embedding2 = model.encode(post["sentences"])
            post_st_score = vector_calc(v, v2, post_embedding, post_embedding2)

            distances.append([post_categories[j], calc_distance(resume_st_score, post_st_score)])

        scores = sorted(distances, key=lambda x: x[1], reverse=False)

        print(scores[:5])
        res["baseline_2"] = [x[0] for x in scores[:5]]

    with open('baseline2.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(data), file=text_file)


def add_annotations():
    filename = "baseline2.json"
    data_file = open(filename, encoding='utf-8')
    data = json.loads(data_file.read())
    data_file.close()

    for res in data:
        res["baseline1_annotation1"] = ""
        res["baseline1_annotation2"] = ""
        res["baseline2_annotation1"] = ""
        res["baseline2_annotation2"] = ""

    with open('ready_data.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(data), file=text_file) 


def baseline1bis():
    filename = "eval_data3.json"
    data_file = open(filename, encoding='utf-8')
    data = json.loads(data_file.read())
    data_file.close()
    for i, res in enumerate(data):
        print(i)
        resume_text = res.get("resume")
        jobs = [] + res["post_categories"]
        for jj in res["predicted_post_categories"]:
            if jj not in jobs:
                jobs.append(jj)

        orig_embedding = model.encode(jobs)

        resume = p.get_data(resume_text)
        resume_embedding = model.encode(resume["nouns"] + resume["nouns phrases"] + resume["sentences"])
        st_scores = cos_sim(orig_embedding, resume_embedding)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]
        result = []
        for k, name in enumerate(jobs):
            result.append([name, m[k]])
        scores = sorted(result, key=lambda x: x[1], reverse=True)

        print(scores[:5])
        res["baseline_1"] = [x[0] for x in scores[:5]]

    with open('eval_data3bis.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(data), file=text_file)


# baseline1()
# baseline2()
# add_annotations()
baseline1bis()
