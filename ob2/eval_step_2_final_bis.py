from cv_analyzer import *


filename = "eval_data2bis.json"
data_file = open(filename, encoding='utf-8')
data = json.loads(data_file.read())
data_file.close()

for i, cv in enumerate(data):
    resume = cv["resume"]

    jobs = [] + cv["post_categories"]
    for jj in cv["predicted_post_categories"]:
        if jj not in jobs:
            jobs.append(jj)

    result = cv_get_results(resume, jobs=jobs)
    cv["predicted_categories_for_curriculum-post_association"] = [x[4] for x in result]
    print(i, result)
    del cv["index_of_associated_post"]

with open('eval_data3.json', "w", encoding="utf-8") as text_file:
    print(json.dumps(data), file=text_file)
