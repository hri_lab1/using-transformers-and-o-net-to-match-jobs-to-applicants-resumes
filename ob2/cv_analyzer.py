# import csv
import json

import torch
from textblob import Blobber
from textblob import Word
from textblob.taggers import NLTKTagger
from skills_query import QueryMysql

from sentence_transformers import SentenceTransformer
from sentence_transformers.util import cos_sim

settings = {"model": "all-mpnet-base-v2", "skills": "skill_embeddings.pt", "jobs": "titles_embeddings.pt",
            "tasks": "task_embeddings.pt", "tools": "tools_embeddings.pt"}

# settings = {"model": "all-MiniLM-L6-v2", "skills": "skill_embeddings-2.pt", "jobs": "titles_embeddings-2.pt",
# "tasks": "task_embeddings-2.pt", "tools": "tools_embeddings-2.pt"}

model = SentenceTransformer(settings["model"], device='cuda' if torch.cuda.is_available() else "cpu")


class Parser:
    def __init__(self):
        self.tb = Blobber(pos_tagger=NLTKTagger())

    def get_data(self, sentence):
        data = {}
        nouns = []
        nouns_phrases = []
        numbers = []
        result = self.tb(sentence)
        data['sentences'] = [str(x) for x in result.sentences]

        for sentence in result.sentences:
            nouns_phrases.extend(list(sentence.noun_phrases))

            for tag in sentence.tags:
                if tag[1].startswith('N'):
                    w = Word(tag[0]).lemmatize()
                    nouns.append(str(w))
                if tag[1].startswith('CD'):
                    w = Word(tag[0]).lemmatize()
                    numbers.append(str(w))

        data['nouns'] = nouns
        data['numbers'] = numbers
        data['nouns phrases'] = nouns_phrases
        return data


def get_score(db_q, emb_sentences, jobs):
    skill_embeddings = torch.load(settings["skills"])
    job_codes = db_q.get_job_codes() if len(jobs) == 0 else jobs
    scores = []
    # sentences = cv_data['nouns phrases'] + cv_data['nouns']

    # emb_sentences = model.encode(sentences)

    for job_code in job_codes:
        tech_skills = db_q.get_tech_skills(job_code[0])
        skills_names = [x[1] for x in tech_skills]
        embeddings = skill_embeddings.get(job_code[0])
        if embeddings is None:
            scores.append((0, 0, 0, job_code[0], job_code[1]))
            continue
        st_scores = cos_sim(embeddings, emb_sentences)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]
        # ind = [int(x) for x in maximus.indices]
        # a = [sentences[x] for x in ind]

        score = 0
        tot_score = 0

        for skill in tech_skills:
            if skill[3] == 'Y':
                tot_score += 1
            else:
                tot_score += 0.75

        for i in range(len(skills_names)):
            if m[i] > 0.65:
                if tech_skills[i][3] == 'Y':
                    score += 1
                else:
                    score += 0.75

        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))

    return scores


def get_st_scores(emb_sentences, db_entities_list, db_q, query_function, jobs):
    embeddings = model.encode(db_entities_list)
    # emb_sentences = model.encode(sentences)

    scores = cos_sim(embeddings, emb_sentences)
    maximus = torch.max(scores, 1)
    m = [float(x) for x in maximus.values]
    # ind = [int(x) for x in maximus.indices]
    # a = [sentences[x] for x in ind]

    job_codes = db_q.get_job_codes() if len(jobs) == 0 else jobs
    scores = []
    for job_code in job_codes:

        skills = query_function(job_code[0])
        score = 0
        tot_score = 0

        for i, skill in enumerate(skills):
            tot_score += float(skill[1])
            score += float(skill[1]) if m[i] > 0.65 else 0

        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))

    return scores


def get_task_scores(db_q, cv_data, jobs):
    task_embeddings = torch.load(settings["tasks"])
    job_codes = db_q.get_job_codes() if len(jobs) == 0 else jobs
    scores = []
    sentences = cv_data['nouns phrases'] + cv_data["sentences"]  # + cv_data['nouns']

    emb_sentences = model.encode(sentences)

    for job_code in job_codes:
        tasks = db_q.get_tasks(job_code[0])
        # task_names = [x[3] for x in tasks]
        embeddings = task_embeddings.get(job_code[0])
        if embeddings is None:
            scores.append((0, 0, 0, job_code[0], job_code[1]))
            continue
        st_scores = cos_sim(embeddings, emb_sentences)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]
        # ind = [int(x) for x in maximus.indices]
        # a = [sentences[x] for x in ind]
        score = 0
        tot_score = 0
        for i, task in enumerate(tasks):
            tot_score += float(task[1])
            score += float(task[1]) if m[i] > 0.65 else 0
        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))
    return scores


def get_tools_scores(db_q, cv_data, jobs):
    tools_embeddings = torch.load(settings["tools"])
    job_codes = db_q.get_job_codes() if len(jobs) == 0 else jobs
    scores = []
    sentences = cv_data['nouns phrases'] + cv_data['nouns'] + cv_data["sentences"]

    emb_sentences = model.encode(sentences)

    for job_code in job_codes:
        tools = db_q.get_tools(job_code[0])
        tool_names = [x[1] for x in tools]
        embeddings = tools_embeddings.get(job_code[0])
        if embeddings is None:
            scores.append((0, 0, 0, job_code[0], job_code[1]))
            continue
        st_scores = cos_sim(embeddings, emb_sentences)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]
        # ind = [int(x) for x in maximus.indices]
        # a = [sentences[x] for x in ind]

        score = 0
        tot_score = 0

        for i in range(len(tool_names)):
            tot_score += 1
            if m[i] > 0.65:
                score += 1

        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))

    return scores


def cv_get_results(resume, q=QueryMysql(), p=Parser(), jobs=None):
    if jobs is None or len(jobs) == 0:
        jobs = []
    else:
        jobs = json.loads(get_st_job_titles(jobs))
        jobs = list(set(jobs["codes"]))

    skills_names = q.get_skills_descriptions()
    skills_names2 = [x[1] for x in skills_names]
    skills_names = [x[0] for x in skills_names]

    knowledge_names = q.get_knowledge()
    knowledge_names2 = [x[3] for x in knowledge_names]
    knowledge_names = [x[2] for x in knowledge_names]

    abilities_names = q.get_abilities()
    abilities_names2 = [x[3] for x in abilities_names]
    abilities_names = [x[2] for x in abilities_names]

    work_activities = q.get_work_activities()
    work_activities_names = [x[2] for x in work_activities]
    work_activities_names2 = [x[3] for x in work_activities]

    cv_data = p.get_data(resume)

    data_to_send = model.encode(cv_data['nouns phrases'] + cv_data['nouns'])
    data_to_send2 = model.encode(cv_data['sentences'])

    score1 = get_score(q, data_to_send, jobs)

    score2 = get_st_scores(data_to_send, skills_names, q, q.get_skills_im, jobs)
    score2b = get_st_scores(data_to_send2, skills_names2, q, q.get_skills_im, jobs)

    score3 = get_st_scores(data_to_send, knowledge_names, q, q.get_knowledge, jobs)
    score3b = get_st_scores(data_to_send2, knowledge_names2, q, q.get_knowledge, jobs)

    score4 = get_st_scores(data_to_send, abilities_names, q, q.get_abilities, jobs)
    score4b = get_st_scores(data_to_send2, abilities_names2, q, q.get_abilities, jobs)

    score5 = get_st_scores(data_to_send, work_activities_names, q, q.get_work_activities, jobs)
    score5b = get_st_scores(data_to_send2, work_activities_names2, q, q.get_work_activities, jobs)

    score6 = get_task_scores(q, cv_data, jobs)

    score7 = get_tools_scores(q, cv_data, jobs)

    score_tot = []

    for j in range(len(score1)):
        s = (
            score1[j][0] + score2[j][0] + score3[j][0] + score4[j][0] + score5[j][0] + score6[j][0] + score7[j][0] +
            score2b[j][0] + score3b[j][0] + score4b[j][0] + score5b[j][0],
            score1[j][1] + score2[j][1] + score3[j][1] + score4[j][1] + score5[j][1] + score6[j][1] + score7[j][1] +
            score2b[j][1] + score3b[j][1] + score4b[j][1] + score5b[j][1],
            score1[j][2] + score2[j][2] + score3[j][2] + score4[j][2] + score5[j][2] + score6[j][2] + score7[j][2] +
            score2b[j][2] + score3b[j][2] + score4b[j][2] + score5b[j][2],
            score1[j][3],
            score1[j][4]
        )
        score_tot.append(s)

    score_tot = sorted(score_tot, key=lambda x: x[0], reverse=True)
    return score_tot[:5]


def get_st_job_titles(jobs):
    if len(jobs) == 0:
        return []

    jobs_titles = torch.load(settings["jobs"])

    titles = jobs_titles["titles"]
    codes = jobs_titles["codes"]
    emb_sentences = jobs_titles["emb_titles"]
    embeddings = model.encode(jobs)

    scores = cos_sim(embeddings, emb_sentences)
    # print(scores.size())
    maximus = torch.max(scores, 1)
    # print(maximus.values.size())
    # print(maximus.indices)
    indices = [int(x) for x in maximus.indices]
    obt_titles = [titles[x] for x in indices]
    obt_codes = [codes[x] for x in indices]
    codes = [[codes[x], titles[x]] for x in indices]
    results = {}
    for i in range(len(jobs)):
        results[jobs[i]] = [obt_titles[i], obt_codes[i]]
    results["codes"] = codes
    return json.dumps(results)
