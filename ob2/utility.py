import json
import torch
from skills_query import QueryMysql
from sentence_transformers import SentenceTransformer

model = SentenceTransformer('all-mpnet-base-v2', device='cuda' if torch.cuda.is_available() else "cpu")


def get_english_job_posts():
    with open("careerbuilder_uk__10k_data.json", encoding='utf-8') as data_file:
        data = json.loads(data_file.read())
        jobs_results = []
        for job_post in data:
            if (job_post.get("job_post_lang") is not None
                and job_post.get("job_post_lang").lower().startswith("en-")) or (
                    job_post.get("inferred_iso2_lang_code") is not None
                    and job_post.get("inferred_iso2_lang_code").lower().startswith("en")):
                jobs_results.append(job_post)
        with open('job_posts.json', "w", encoding="utf-8") as text_file:
            print(json.dumps(jobs_results, indent=2, ensure_ascii=True), file=text_file)


def save_all_tech_skills_embeddings(db_q):
    codes = db_q.get_all_tech_skills()
    skills = [x[0] for x in codes]
    print(skills)

    skill_embeddings = model.encode(skills)
    torch.save(skill_embeddings, 'baseline2/all_skills_embeddings.pt')


def save_all_tools_embeddings(db_q):
    codes = db_q.get_all_tools()
    tools = [x[0] for x in codes]
    print(tools)

    tools_embeddings = model.encode(tools)
    torch.save(tools_embeddings, 'baseline2/all_tools_embeddings.pt')


def save_all_tasks_embeddings(db_q):
    codes = db_q.get_all_tasks()
    tasks = [x[0] for x in codes]
    print(tasks)

    tasks_embeddings = model.encode(tasks)
    torch.save(tasks_embeddings, 'baseline2/all_tasks_embeddings.pt')


def get_vectors(q):
    skills_names = q.get_skills_descriptions()
    skills_names2 = [x[1] for x in skills_names]
    skills_names = [x[0] for x in skills_names]
    skills_names_embeddings = model.encode(skills_names)
    torch.save(skills_names_embeddings, 'baseline2/skill_names_embeddings.pt')
    skills_names2_embeddings = model.encode(skills_names2)
    torch.save(skills_names2_embeddings, 'baseline2/skill_names2_embeddings.pt')

    knowledge_names = q.get_knowledge()
    knowledge_names2 = [x[3] for x in knowledge_names]
    knowledge_names = [x[2] for x in knowledge_names]
    knowledge_names_embeddings = model.encode(knowledge_names)
    torch.save(knowledge_names_embeddings, 'baseline2/knowledge_names_embeddings.pt')
    knowledge_names2_embeddings = model.encode(knowledge_names2)
    torch.save(knowledge_names2_embeddings, 'baseline2/knowledge_names2_embeddings.pt')

    abilities_names = q.get_abilities()
    abilities_names2 = [x[3] for x in abilities_names]
    abilities_names = [x[2] for x in abilities_names]
    abilities_names_embeddings = model.encode(abilities_names)
    torch.save(abilities_names_embeddings, 'baseline2/abilities_names_embeddings.pt')
    abilities_names2_embeddings = model.encode(abilities_names2)
    torch.save(abilities_names2_embeddings, 'baseline2/abilities_names2_embeddings.pt')

    work_activities = q.get_work_activities()
    work_activities_names = [x[2] for x in work_activities]
    work_activities_names2 = [x[3] for x in work_activities]
    work_activities_names_embeddings = model.encode(work_activities_names)
    torch.save(work_activities_names_embeddings, 'baseline2/work_activities_names_embeddings.pt')
    work_activities_names2_embeddings = model.encode(work_activities_names2)
    torch.save(work_activities_names2_embeddings, 'baseline2/work_activities_names2_embeddings.pt')


# save_all_tech_skills_embeddings(QueryMysql())
# save_all_tools_embeddings(QueryMysql())
# save_all_tasks_embeddings(QueryMysql())
get_vectors(QueryMysql())
