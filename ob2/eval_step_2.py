from cv_analyzer import *
import json


def save_prediction(resumes):
    for i, resume in enumerate(resumes):
        file_name = "job_posts_sample" + str(i) + ".json"
        data_file = open(file_name, encoding='utf-8')
        data = json.loads(data_file.read())
        results = []
        for post in data:
            text1 = post.get("job_description")
            text2 = post.get("job_requirements")
            if text1 is not None:
                text = text1 + " - "
            else:
                text = ""
            if text2 is not None:
                text += text2
            result = {"post": post, "predicted_categories": cv_get_results(text)}
            print(result)
            results.append(result)
        filename = "job_posts_sample_eval" + str(i) + ".json"
        with open(filename, "w", encoding="utf-8") as text_file:
            print(json.dumps(results), file=text_file)


def get_resume_predictions():
    filename = "five_unique_resumes.json"
    data_file = open(filename, encoding='utf-8')
    data = json.loads(data_file.read())
    data_file.close()
    jobs = ["Data Science", "Advocate", "Mechanical Engineer"]
    resumes = []
    for key in data:
        if key in jobs:
            resumes.append(data.get(key)[4])

    save_prediction(resumes)
    results = []
    for i, resume in enumerate(resumes):
        filename = "job_posts_sample_eval" + str(i) + ".json"
        data_file = open(filename, encoding='utf-8')
        data = json.loads(data_file.read())
        data_file.close()
        k = []
        for post in data:
            if post["post"].get("category") is not None:
                k.append(post["post"].get("category"))

        z = [x["predicted_categories"] for x in data]
        jj = [x for y in z for x in y]

        jobs = [y[4] for y in jj]
        jobs.extend(k)
        unique_jobs = []
        for job in jobs:
            if job not in unique_jobs:
                unique_jobs.append(job)
        print(len(unique_jobs))
        print(unique_jobs)

        result = {"posts": data, "resume": resume, "predicted_categories": cv_get_results(resume, jobs=unique_jobs)}
        print(result)
        results.append(result)

    filename = "job_posts_toy_sample_eval.json"
    with open(filename, "w", encoding="utf-8") as text_file:
        print(json.dumps(results), file=text_file)


def analyze_results():
    filename = "job_posts_toy_sample_eval.json"
    data_file = open(filename, encoding='utf-8')
    data = json.loads(data_file.read())
    data_file.close()
    for r in data:
        posts = r.get("posts")
        resume = r.get("resume")
        predicted = r.get("predicted_categories")
        for prediction in predicted:
            p = prediction[4]
            for i, post in enumerate(posts):
                pred_cats = post.get("predicted_categories")
                pp = [x[4] for x in pred_cats]
                if p in pp:
                    prediction.append({"post": i, "position": pp.index(p), "job_title": post["post"].get("job_title"),
                                       "category": post["post"].get("category")})
                print(p, post["post"].get("category"), p == post["post"].get("category"))
                if p == post["post"].get("category"):
                    prediction.append({"post": i, "position": "post category", "job_title": post["post"].get("job_title"),
                                       "category": post["post"].get("category")})

    #print(data)
    filename = "job_posts_toy_sample_eval_complete.json"
    with open(filename, "w", encoding="utf-8") as text_file:
        print(json.dumps(data), file=text_file)


get_resume_predictions()
analyze_results()
