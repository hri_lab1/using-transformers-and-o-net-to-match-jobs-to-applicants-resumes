# install for GPU -> pip install torch torchvision torchaudio --extra-index-url https://download.pytorch.org/whl/cu113
# install for CPU -> pip install torch torchvision torchaudio
# install -> pip install sentence-transformers
# install -> pip install textblob
# install -> python -m textblob.download_corpora

import csv
import json

import torch
from textblob import Blobber
from textblob import Word
from textblob.taggers import NLTKTagger
from skills_query import QueryMysql

from sentence_transformers import SentenceTransformer
from sentence_transformers.util import cos_sim

settings = {"model": "all-mpnet-base-v2", "skills": "skill_embeddings.pt", "jobs": "titles_embeddings.pt",
            "tasks": "task_embeddings.pt", "tools": "tools_embeddings.pt"}

model = SentenceTransformer(settings["model"], device='cuda' if torch.cuda.is_available() else "cpu")


class Parser:
    def __init__(self):
        self.tb = Blobber(pos_tagger=NLTKTagger())

    def get_data(self, sentence):
        data = {}
        nouns = []
        nouns_phrases = []
        numbers = []
        result = self.tb(sentence)
        data['sentences'] = [str(x) for x in result.sentences]

        for sentence in result.sentences:
            nouns_phrases.extend(list(sentence.noun_phrases))

            for tag in sentence.tags:
                if tag[1].startswith('N'):
                    w = Word(tag[0]).lemmatize()
                    nouns.append(str(w))
                if tag[1].startswith('CD'):
                    w = Word(tag[0]).lemmatize()
                    numbers.append(str(w))

        data['nouns'] = nouns
        data['numbers'] = numbers
        data['nouns phrases'] = nouns_phrases
        return data


def file_opener(q):
    file = open("five_unique_resumes.tsv", encoding="utf8")
    cv = csv.reader(file, delimiter="\t")

    jobs = []
    job_first_index = []
    job_last_index = []
    resumes = []
    cvs_list = []

    for i, row in enumerate(cv):
        cvs_list.append(row)
        resumes.append(row[1])
        if row[0] not in jobs:
            job_first_index.append(i)
            job_last_index.append(i - 1)
            jobs.append(row[0])
    job_last_index.append(len(resumes) - 1)
    jobs.pop(0)
    job_first_index.pop(0)
    job_last_index.pop(0)
    job_last_index.pop(0)
    resumes.pop(0)
    cvs_list.pop(0)

    cv_sum = 0
    jobs_data = []
    for i, job in enumerate(jobs):
        onetsoc_code = q.get_job_code(job)
        cv_sum += job_last_index[i] - job_first_index[i] + 1
        cv_number = job_last_index[i] - job_first_index[i] + 1
        jobs_data.append((cv_number, job, job_first_index[i], job_last_index[i], onetsoc_code))
        print(job_last_index[i] - job_first_index[i] + 1, job, '(', job_first_index[i], '-', job_last_index[i], ')')
    print('Tot. resumes: ', cv_sum)
    return resumes, cvs_list, jobs_data, jobs


def get_score(db_q, cv_data):
    skill_embeddings = torch.load(settings["skills"])
    job_codes = db_q.get_job_codes()
    scores = []
    sentences = cv_data['nouns phrases'] + cv_data['nouns']
    print('sentences' + str(len(sentences)))
    emb_sentences = model.encode(sentences)

    for job_code in job_codes:
        tech_skills = db_q.get_tech_skills(job_code[0])
        skills_names = [x[1] for x in tech_skills]
        embeddings = skill_embeddings.get(job_code[0])
        if embeddings is None:
            scores.append((0, 0, 0, job_code[0], job_code[1]))
            continue

        st_scores = cos_sim(embeddings, emb_sentences)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]

        score = 0
        tot_score = 0

        for skill in tech_skills:
            if skill[3] == 'Y':
                tot_score += 1.0
            else:
                tot_score += 0.75

        for i in range(len(skills_names)):
            if m[i] > 0.65:
                if tech_skills[i][3] == 'Y':
                    score += 1.0
                else:
                    score += 0.75

        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))

    return scores


def get_st_scores(sentences, db_entities_list, db_q, query_function):
    embeddings = model.encode(db_entities_list)
    emb_sentences = model.encode(sentences)
    scores = cos_sim(embeddings, emb_sentences)
    maximus = torch.max(scores, 1)
    m = [float(x) for x in maximus.values]

    job_codes = db_q.get_job_codes()
    scores = []
    for job_code in job_codes:

        skills = query_function(job_code[0])
        score = 0
        tot_score = 0

        for i, skill in enumerate(skills):
            tot_score += float(skill[1])
            score += float(skill[1]) if m[i] > 0.65 else 0

        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))

    return scores


def get_task_scores(db_q, cv_data, jobs):
    task_embeddings = torch.load(settings["tasks"])
    job_codes = db_q.get_job_codes() if len(jobs) == 0 else jobs
    scores = []
    sentences = cv_data['nouns phrases'] + cv_data['nouns'] + cv_data["sentences"]
    emb_sentences = model.encode(sentences)
    for job_code in job_codes:
        tasks = db_q.get_tasks(job_code[0])
        embeddings = task_embeddings.get(job_code[0])
        if embeddings is None:
            scores.append((0, 0, 0, job_code[0], job_code[1]))
            continue
        st_scores = cos_sim(embeddings, emb_sentences)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]
        score = 0
        tot_score = 0
        for i, task in enumerate(tasks):
            tot_score += float(task[1])
            score += float(task[1]) if m[i] > 0.65 else 0
        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))
    return scores


def get_tools_scores(db_q, cv_data, jobs):
    tools_embeddings = torch.load(settings["tools"])
    job_codes = db_q.get_job_codes() if len(jobs) == 0 else jobs
    scores = []
    sentences = cv_data['nouns phrases'] + cv_data['nouns'] + cv_data["sentences"]

    emb_sentences = model.encode(sentences)

    for job_code in job_codes:
        tools = db_q.get_tools(job_code[0])
        tool_names = [x[1] for x in tools]
        embeddings = tools_embeddings.get(job_code[0])
        if embeddings is None:
            scores.append((0, 0, 0, job_code[0], job_code[1]))
            continue
        st_scores = cos_sim(embeddings, emb_sentences)
        maximus = torch.max(st_scores, 1)
        m = [float(x) for x in maximus.values]

        score = 0
        tot_score = 0

        for i in range(len(tool_names)):
            tot_score += 1
            if m[i] > 0.65:
                score += 1

        points = 0 if tot_score == 0 or score == 0 else float(score / tot_score) + 0.5 * float(score)
        scores.append((points, float(score), float(tot_score), job_code[0], job_code[1]))

    return scores


def cv_get_results(p, q, resumes, data):
    results = {}
    scores = []

    scores_tot = []
    skills_names = q.get_skills_descriptions()
    skills_names2 = [x[1] for x in skills_names]
    skills_names = [x[0] for x in skills_names]

    knowledge_names = q.get_knowledge()
    knowledge_names2 = [x[3] for x in knowledge_names]
    knowledge_names = [x[2] for x in knowledge_names]

    abilities_names = q.get_abilities()
    abilities_names2 = [x[3] for x in abilities_names]
    abilities_names = [x[2] for x in abilities_names]

    work_activities = q.get_work_activities()
    work_activities_names = [x[2] for x in work_activities]
    work_activities_names2 = [x[3] for x in work_activities]
    valutation_results = []

    for job in data:
        for i in range(job[2] - 1, job[2] + 4):

            cv_data = p.get_data(resumes[i])
            score1 = get_score(q, cv_data)
            scores.append(score1)

            data_to_send = cv_data['nouns phrases'] + cv_data['nouns']
            data_to_send2 = cv_data['sentences']
            score2 = get_st_scores(data_to_send, skills_names, q, q.get_skills_im)
            score2b = get_st_scores(data_to_send2, skills_names2, q, q.get_skills_im)
            score3 = get_st_scores(data_to_send, knowledge_names, q, q.get_knowledge)
            score3b = get_st_scores(data_to_send2, knowledge_names2, q, q.get_knowledge)
            score4 = get_st_scores(data_to_send, abilities_names, q, q.get_abilities)
            score4b = get_st_scores(data_to_send2, abilities_names2, q, q.get_abilities)
            score5 = get_st_scores(data_to_send, work_activities_names, q, q.get_work_activities)
            score5b = get_st_scores(data_to_send2, work_activities_names2, q, q.get_work_activities)
            score6 = get_task_scores(q, cv_data, [])
            score7 = get_tools_scores(q, cv_data, [])
            score_tot = []

            for j in range(len(score1)):
                s = (
                    score1[j][0] + score2[j][0] + score3[j][0] + score4[j][0] + score5[j][0] + score6[j][0] +
                    score7[j][0] + score2b[j][0] + score3b[j][0] + score4b[j][0] + score5b[j][0],
                    score1[j][1] + score2[j][1] + score3[j][1] + score4[j][1] + score5[j][1] + score6[j][1] +
                    score7[j][1] + score2b[j][1] + score3b[j][1] + score4b[j][1] + score5b[j][1],
                    score1[j][2] + score2[j][2] + score3[j][2] + score4[j][2] + score5[j][2] + score6[j][2] +
                    score7[j][2] + score2b[j][2] + score3b[j][2] + score4b[j][2] + score5b[j][2],
                    score1[j][3],
                    score1[j][4]
                )
                score_tot.append(s)

            score_tot = sorted(score_tot, key=lambda x: x[0], reverse=True)

            scores_tot.append(score_tot[:5])
            print(job[1], end=' ')
            print(score_tot[:5])

            val_res = {"resume_text": resumes[i], "original_job": job[1],
                       "predicted_jobs": [(x[3], x[4]) for x in score_tot[:5]],
                       "annotation_1": "",
                       "annotation_2": ""}
            valutation_results.append(val_res)

        results[job[1]] = scores_tot
        scores = []
        scores_tot = []

    with open('top_5-jobs.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(results), file=text_file)
    with open('valutation_results.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(valutation_results), file=text_file)


def get_st_job_titles(q, jobs):
    jobs_titles = q.get_job_titles()

    titles = [x[1] for x in jobs_titles]
    codes = [x[0] for x in jobs_titles]
    embeddings = model.encode(jobs)
    emb_sentences = model.encode(titles)
    scores = cos_sim(embeddings, emb_sentences)
    print(scores.size())
    maximus = torch.max(scores, 1)
    print(maximus.values.size())
    print(maximus.indices)
    indices = [int(x) for x in maximus.indices]
    obt_titles = [titles[x] for x in indices]
    obt_codes = [codes[x] for x in indices]
    results = {}
    for i in range(len(jobs)):
        results[jobs[i]] = [obt_titles[i], obt_codes[i]]
    with open('jobs_titles.json', "w", encoding="utf-8") as text_file:
        print(json.dumps(results), file=text_file)
    return results


def clean_resume_list(cvs_list):
    lista = []
    cv = []
    for res in cvs_list:
        if res[1] not in lista:
            lista.append(res[1])
            cv.append(res)
    print(len(cv))
    with open('unique_resumes.tsv', "w", encoding="utf-8") as text_file:
        print("Category\tResume", file=text_file)
        for resume in cv:
            print(resume[0] + "\t" + resume[1], file=text_file)


def get_longest_resumes(resumes):
    cv = {}
    categories = []
    for res in resumes:

        if res[0] not in categories:
            cv[res[0]] = []
            categories.append(res[0])
        cv[res[0]].append(res[1])

    cv2 = {}
    for key in cv:
        if len(cv[key]) >= 5:
            sorted_list = list(sorted(cv[key], key=len, reverse=True))
            cv2[key] = sorted_list[:5]

    with open('five_unique_resumes.tsv', "w", encoding="utf-8") as text_file:
        print("Category\tResume", file=text_file)
        for key in cv2:
            for res in cv2[key]:
                print(key + "\t" + res, file=text_file)


def save_tech_skills_embeddings(db_q):
    job_codes = db_q.get_job_codes()
    skill_embeddings = {}

    for job_code in job_codes:
        tech_skills = db_q.get_tech_skills(job_code[0])

        skills_names = [x[1] for x in tech_skills]
        print(len(skills_names))
        if len(skills_names) > 0:
            skill_embeddings[job_code[0]] = model.encode(skills_names)

    torch.save(skill_embeddings, 'skill_embeddings.pt')


def main():
    q = QueryMysql()
    p = Parser()
    resumes, cvs_list, data, jobs = file_opener(q)

    cv_get_results(p, q, resumes, data)

    # confront of jobs names with onet ones
    results = get_st_job_titles(q, jobs)
    print(results)


main()
