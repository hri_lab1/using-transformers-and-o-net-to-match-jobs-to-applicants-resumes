import torch
from skills_query import QueryMysql
from sentence_transformers import SentenceTransformer

model = SentenceTransformer('all-mpnet-base-v2', device='cuda' if torch.cuda.is_available() else "cpu")


def save_st_job_titles(q):
    job_titles = q.get_job_titles()
    job_emb_titles = {}
    titles = [x[1] for x in job_titles]
    codes = [x[0] for x in job_titles]
    emb_titles = model.encode(titles)
    job_emb_titles["titles"] = titles
    job_emb_titles["codes"] = codes
    job_emb_titles["emb_titles"] = emb_titles
    torch.save(job_emb_titles, 'titles_embeddings.pt')


def save_tech_skills_embeddings(db_q):
    job_codes = db_q.get_job_codes()
    skill_embeddings = {}

    for job_code in job_codes:
        tech_skills = db_q.get_tech_skills(job_code[0])

        skills_names = [x[1] for x in tech_skills]
        print(len(skills_names))
        if len(skills_names) > 0:
            skill_embeddings[job_code[0]] = model.encode(skills_names)

    torch.save(skill_embeddings, 'skill_embeddings.pt')


def save_tasks_embeddings(db_q):
    job_codes = db_q.get_job_codes()
    task_embeddings = {}

    for job_code in job_codes:
        task_names = db_q.get_tasks(job_code[0])

        task_names = [x[3] for x in task_names]
        print(len(task_names))
        if len(task_names) > 0:
            task_embeddings[job_code[0]] = model.encode(task_names)

    torch.save(task_embeddings, 'task_embeddings.pt')


def save_tools_embeddings(db_q):
    job_codes = db_q.get_job_codes()
    tools_embeddings = {}

    for job_code in job_codes:
        tool_names = db_q.get_tools(job_code[0])

        tool_names = [x[1] for x in tool_names]
        print(len(tool_names))
        if len(tool_names) > 0:
            tools_embeddings[job_code[0]] = model.encode(tool_names)

    torch.save(tools_embeddings, 'tools_embeddings.pt')


save_st_job_titles(QueryMysql())
save_tech_skills_embeddings(QueryMysql())
save_tasks_embeddings(QueryMysql())
save_tools_embeddings(QueryMysql())
